#geometry data
inlet_gap = 0.01
belt_length = 0.3
em_x0 = 0.02
em_y0 = 0.005
em_height = 0.02
em_length = 0.05
width = 0.1

#aluminium data at 1200K
#https://link.springer.com/article/10.1007/s11661-017-4053-6
density = 2311
dyn_viscosity = 0.0008
viscosity = dyn_viscosity/density
melting_point = 933
surface_tension = 0.84/2
el_conductivity = 3703703
latent_heat = 360000
heat_conductivity = 98
heat_capacity = 1122

#contact angles
belt_angle = 160
side_angle = 180

#magnetic field strength in T
mag_field_strength = 1

#belt speed in m/s, cooling rate in W/m2
belt_cooling_rate = -69231551/2
side_cooling_rate = 0
belt_speed = 0.2
#belt cooling rate estimate to obtain fully solid strip at the end of the belt:
#-10*heat_capacity*inlet_flowrate*(inlet_temperature-melting_point)/(width*belt_length)

#inlet data, flowrate in kg/s
inlet_temperature = 1200
inlet_flowrate = 0.4622
#inlet_flowrate estimate to obtain specific average strip_thickness:
#inlet_flowrate = strip_thickness*density*width*belt_speed

#characteristic mesh element sizes
dx_inlet = 0.001
dx_belt = 0.002
dy_belt = 0.001
dy_em = 0.002
dz = 0.002
