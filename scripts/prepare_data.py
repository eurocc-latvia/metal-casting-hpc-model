from shutil import copyfile
import os
from decimal import Decimal

demodir = os.getcwd()

if os.path.exists(demodir+"/user_data.py"):
	copyfile(demodir+"/user_data.py",demodir+"/scripts/user_data.py")

import user_data

with open('scripts/salome_input_template.py','r') as salome_py:
	salome_lines = salome_py.readlines()

if os.path.exists("elmer_data"):
	os.remove("elmer_data")

if os.path.exists("salome_input.py"):
	os.remove("salome_input.py")

if os.path.exists("openfoam_data"):
	os.remove("openfoam_data")

elmer_data = open('elmer_data','a')
salome_py = open('salome_input.py','a')
of_data = open('openfoam_data','a')

#calculate derived variables
nw = -0.5*user_data.width
pw = 0.5*user_data.width
em_y1 = -user_data.em_y0-user_data.em_height
em_x1 = user_data.em_x0+user_data.em_length
em_zone_x1 = user_data.em_length+2*user_data.em_x0
h = user_data.inlet_flowrate/(user_data.density*user_data.width*user_data.belt_speed)
layer = 1.5*h
inlet_height = 4*h
channel_height = 3*h

nx_em = max(int(0.5*user_data.em_length/user_data.dx_inlet),1)
ny_em = max(int(user_data.em_height/user_data.dy_em),1)
nx_inlet = max(int(user_data.inlet_gap/user_data.dx_inlet),1)
nx_em_zone = max(int((em_zone_x1-user_data.inlet_gap)/user_data.dx_inlet),1)
nx_belt = max(int((user_data.belt_length-em_zone_x1)/user_data.dx_belt),1)
ny_inlet = max(int((inlet_height-channel_height)/user_data.dx_inlet),1)
ny_layer = 2*max(int(layer/user_data.dx_inlet),1)
ny_top = max(int((channel_height-layer)/user_data.dx_inlet),1)
nz = max(int(user_data.width/user_data.dz),1)


#write elmer data
elmer_data.write('$ width = %f\n' % user_data.width)
elmer_data.write('$ em_x0 = %f\n' % user_data.em_x0)
elmer_data.write('$ em_length = %f\n' % user_data.em_length)
elmer_data.write('$ mag_field_strength = %f\n' % user_data.mag_field_strength)
elmer_data.write('$ Up = %f\n' % user_data.belt_speed)
elmer_data.close()

#write variables for elmer mesh
salome_py.write('inlet_height = %f\n' % inlet_height)
salome_py.write('inlet_gap = %f\n' % user_data.inlet_gap)
salome_py.write('belt_length = %f\n' % user_data.belt_length)
salome_py.write('channel_height = %f\n' % channel_height)
salome_py.write('em_x0 = %f\n' % user_data.em_x0)
salome_py.write('em_y0 = %f\n' % user_data.em_y0)
salome_py.write('em_height = %f\n' % user_data.em_height)
salome_py.write('em_length = %f\n' % user_data.em_length)
salome_py.write('width = %f\n' % user_data.width)
salome_py.write('belt_speed = %f\n' % user_data.belt_speed)
salome_py.write('dx_inlet = %f\n' % user_data.dx_inlet)
salome_py.write('dx_belt = %f\n' % user_data.dx_belt)
salome_py.write('dy_belt = %f\n' % user_data.dy_belt)
salome_py.write('dy_em = %f\n' % user_data.dy_em)
salome_py.write('dz = %f\n' % user_data.dz)

salome_py.write('nw = %f\n' % nw)
salome_py.write('pw = %f\n' % pw)
salome_py.write('em_y1 = %f\n' % em_y1)
salome_py.write('em_x1 = %f\n' % em_x1)
salome_py.write('em_zone_x1 = %f\n' % em_zone_x1)
salome_py.write('layer = %f\n' % layer)

for line in salome_lines:
	if line == 'cdir0 = \n':
		salome_py.write('cdir0 = %s\n' % ('r'+repr(os.getcwd())))
	else:
		salome_py.write('%s' % line)
salome_py.close()

#write openfoam data
of_data.write('end_time %f;\n' % (2*user_data.belt_length/user_data.belt_speed))
of_data.write('inlet_height %f;\n' % inlet_height)
of_data.write('inlet_gap %f;\n' % user_data.inlet_gap)
of_data.write('belt_length %f;\n' % user_data.belt_length)
of_data.write('channel_height %f;\n' % channel_height)
of_data.write('em_zone_x1 %f;\n' % em_zone_x1)
of_data.write('layer %f;\n' % layer)
of_data.write('em_x0 %f;\n' % user_data.em_x0)
of_data.write('em_y0 %f;\n' % user_data.em_y0)
of_data.write('em_height %f;\n' % user_data.em_height)
of_data.write('em_length %f;\n' % user_data.em_length)
of_data.write('width %f;\n' % user_data.width)
of_data.write('nw %f;\n' % nw)
of_data.write('pw %f;\n' % pw)

of_data.write('density %f;\n' % user_data.density)
of_data.write('viscosity %.3E;\n' % Decimal(user_data.viscosity))
of_data.write('tsolid %f;\n' % user_data.melting_point)
of_data.write('surface_tension %f;\n' % user_data.surface_tension)
of_data.write('el_conductivity %f;\n' % user_data.el_conductivity)
of_data.write('latent_heat %f;\n' % user_data.latent_heat)
of_data.write('heat_conductivity %f;\n' % user_data.heat_conductivity)
of_data.write('heat_capacity %f;\n' % user_data.heat_capacity)

of_data.write('belt_angle %f;\n' % user_data.belt_angle)
of_data.write('side_angle %f;\n' % user_data.side_angle)

of_data.write('belt_cooling_rate %f;\n' % (user_data.belt_cooling_rate/user_data.heat_conductivity))
of_data.write('side_cooling_rate %f;\n' % (user_data.side_cooling_rate/user_data.heat_conductivity))
of_data.write('u_belt %f;\n' % user_data.belt_speed)
of_data.write('T_inlet %f;\n' % user_data.inlet_temperature)
of_data.write('inlet_v %f;\n' % (-user_data.inlet_flowrate/
								(user_data.density*
								(user_data.inlet_gap*user_data.width))))

of_data.write('nx_inlet %i;\n' % nx_inlet)
of_data.write('nx_em_zone %i;\n' % nx_em_zone)
of_data.write('nx_belt %i;\n' % nx_belt)
of_data.write('ny_inlet %i;\n' % ny_inlet)
of_data.write('ny_layer %i;\n' % ny_layer)
of_data.write('ny_top %i;\n' % ny_top)
of_data.write('nz %i;\n' % nz)

of_data.close()
