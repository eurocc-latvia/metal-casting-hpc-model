em_elem_size = dy_em
layer_elem_size = (dx_belt+dy_belt+dz)/3
air0_elem_size = dx_belt
of1_elem_size = 2*dx_belt
far_elem_size = 5*dx_belt
belt0_elem_size = 0.5*(dx_belt+dy_belt+dz)/3

min_elem_size = 0.0001
max_elem_size = 0.1

import sys
import salome
import os

salome.salome_init()
import salome_notebook
notebook = salome_notebook.NoteBook()

cdir0 = 
cdir = cdir0.replace('\\\\', '\\')

sys.path.insert(0, cdir)

###
### GEOM component
###

import GEOM
from salome.geom import geomBuilder
import math
import SALOMEDS

geompy = geomBuilder.New()

#create points, edges and faces
O = geompy.MakeVertex(0, 0, 0)
OX = geompy.MakeVectorDXDYDZ(1, 0, 0)
OY = geompy.MakeVectorDXDYDZ(0, 1, 0)
OZ = geompy.MakeVectorDXDYDZ(0, 0, 1)
point0 = geompy.MakeVertex(0,0,nw)
point1 = geompy.MakeVertex(0,layer,nw)
point2 = geompy.MakeVertex(0,channel_height,nw)
point3 = geompy.MakeVertex(0,inlet_height,nw)
point4 = geompy.MakeVertex(inlet_gap,0,nw)
point5 = geompy.MakeVertex(inlet_gap,layer,nw)
point6 = geompy.MakeVertex(inlet_gap,channel_height,nw)
point7 = geompy.MakeVertex(inlet_gap,inlet_height,nw)
point8 = geompy.MakeVertex(em_zone_x1,0,nw)
point9 = geompy.MakeVertex(em_zone_x1,layer,nw)
point10 = geompy.MakeVertex(em_zone_x1,channel_height,nw)
point11 = geompy.MakeVertex(belt_length,0,nw)
point12 = geompy.MakeVertex(belt_length,layer,nw)
point13 = geompy.MakeVertex(belt_length,channel_height,nw)
point4_0 = geompy.MakeVertex(inlet_gap,-em_y0,nw)
point8_0 = geompy.MakeVertex(em_zone_x1,-em_y0,nw)
point_air0 = geompy.MakeVertex(-0.2,-0.2+em_y1,-0.2+nw)
point_air1 = geompy.MakeVertex(0.2+belt_length,0.2+inlet_height,0.2+pw)
point_em1 = geompy.MakeVertex(em_x0,em_y1,nw)
point_em2 = geompy.MakeVertex(em_x0,-em_y0,nw)
point_em3 = geompy.MakeVertex(em_x1,em_y1,nw)
point_em4 = geompy.MakeVertex(em_x1,-em_y0,nw)
Edge_1 = geompy.MakeEdge(point0, point1)
Edge_2 = geompy.MakeEdge(point0, point4)
Edge_3 = geompy.MakeEdge(point1, point5)
Edge_4 = geompy.MakeEdge(point4, point5)
Face_1 = geompy.MakeFaceWires([Edge_1, Edge_2, Edge_3, Edge_4], 1)
Edge_5 = geompy.MakeEdge(point1, point2)
Edge_6 = geompy.MakeEdge(point2, point6)
Edge_7 = geompy.MakeEdge(point5, point6)
Face_2 = geompy.MakeFaceWires([Edge_5, Edge_3, Edge_6, Edge_7], 1)
Edge_8 = geompy.MakeEdge(point2, point3)
Edge_9 = geompy.MakeEdge(point3, point7)
Edge_10 = geompy.MakeEdge(point6, point7)
Face_3 = geompy.MakeFaceWires([Edge_8, Edge_6, Edge_9, Edge_10], 1)
Edge_11 = geompy.MakeEdge(point4, point8)
Edge_12 = geompy.MakeEdge(point5, point9)
Edge_13 = geompy.MakeEdge(point9, point8)
Face_4 = geompy.MakeFaceWires([Edge_4, Edge_11, Edge_12, Edge_13], 1)
Edge_14 = geompy.MakeEdge(point6, point10)
Edge_15 = geompy.MakeEdge(point10, point9)
Face_5 = geompy.MakeFaceWires([Edge_7, Edge_12, Edge_14, Edge_15], 1)
Edge_16 = geompy.MakeEdge(point8, point11)
Edge_17 = geompy.MakeEdge(point9, point12)
Edge_18 = geompy.MakeEdge(point12, point11)
Face_6 = geompy.MakeFaceWires([Edge_17, Edge_13, Edge_16, Edge_18], 1)
Edge_19 = geompy.MakeEdge(point10, point13)
Edge_20 = geompy.MakeEdge(point13, point12)
Face_7 = geompy.MakeFaceWires([Edge_19, Edge_15, Edge_17, Edge_20], 1)
Edge_21 = geompy.MakeEdge(point_em1, point_em2)
Edge_22 = geompy.MakeEdge(point_em1, point_em3)
Edge_23 = geompy.MakeEdge(point_em2, point_em4)
Edge_24 = geompy.MakeEdge(point_em4, point_em3)
Face_8 = geompy.MakeFaceWires([Edge_21, Edge_22, Edge_23, Edge_24], 1)
Edge_25 = geompy.MakeEdge(point4_0, point4)
Edge_26 = geompy.MakeEdge(point4_0, point8_0)
Edge_27 = geompy.MakeEdge(point8, point8_0)
Face_9 = geompy.MakeFaceWires([Edge_27, Edge_25, Edge_11, Edge_26], 1)

#create air region
Box_1 = geompy.MakeBoxTwoPnt(point_air0, point_air1)

#extrude 2d faces
Extrusion_0 = geompy.MakePrismVecH(Face_1, OZ, width)
Extrusion_1 = geompy.MakePrismVecH(Face_2, OZ, width)
Extrusion_2 = geompy.MakePrismVecH(Face_3, OZ, width)
Extrusion_3 = geompy.MakePrismVecH(Face_4, OZ, width)
Extrusion_4 = geompy.MakePrismVecH(Face_5, OZ, width)
Extrusion_5 = geompy.MakePrismVecH(Face_6, OZ, width)
Extrusion_6 = geompy.MakePrismVecH(Face_7, OZ, width)
Extrusion_7 = geompy.MakePrismVecH(Face_8, OZ, width)
Extrusion_8 = geompy.MakePrismVecH(Face_9, OZ, width)

#unite the geometry
Partition_1 = geompy.MakePartition([Extrusion_0, Extrusion_1, Extrusion_2,
									Extrusion_3, Extrusion_4, Extrusion_5, 
									Extrusion_6, Extrusion_7, Extrusion_8, Box_1],
									[], [], [], geompy.ShapeType["SOLID"], 0, [], 0)

#create part names
openfoam_zone = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(openfoam_zone, [149, 2, 108, 36, 60, 84, 125])
em_system = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(em_system, [166])
air = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(air, [200, 228])
ext = geompy.CreateGroup(Partition_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(ext, [260, 247, 252, 230, 240, 257])
layer0 = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(layer0, [84])
air0 = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(air0, [200])
of1 = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(of1, [108, 2, 36])
ch1 = geompy.CreateGroup(Partition_1, geompy.ShapeType["SOLID"])
geompy.UnionIDs(ch1, [149, 125])
belt0 = geompy.CreateGroup(Partition_1, geompy.ShapeType["FACE"])
geompy.UnionIDs(belt0, [100])
geompy.addToStudy( O, 'O' )
geompy.addToStudy( OX, 'OX' )
geompy.addToStudy( OY, 'OY' )
geompy.addToStudy( OZ, 'OZ' )
geompy.addToStudy( Partition_1, 'Partition_1' )
geompy.addToStudyInFather( Partition_1, openfoam_zone, 'openfoam_zone' )
geompy.addToStudyInFather( Partition_1, em_system, 'em_system' )
geompy.addToStudyInFather( Partition_1, air, 'air' )
geompy.addToStudyInFather( Partition_1, ext, 'ext' )
geompy.addToStudyInFather( Partition_1, layer0, 'layer0' )
geompy.addToStudyInFather( Partition_1, air0, 'air0' )
geompy.addToStudyInFather( Partition_1, of1, 'of1' )
geompy.addToStudyInFather( Partition_1, ch1, 'ch1' )
geompy.addToStudyInFather( Partition_1, belt0, 'belt0' )


###
### SMESH component
###

import  SMESH, SALOMEDS
from salome.smesh import smeshBuilder

smesh = smeshBuilder.New()

#create Elmer mesh
mesh = smesh.Mesh(Partition_1)
NETGEN_1D_2D_3D = mesh.Tetrahedron(algo=smeshBuilder.NETGEN_1D2D3D)
NETGEN_3D_Parameters_1 = NETGEN_1D_2D_3D.Parameters()
NETGEN_3D_Parameters_1.SetMaxSize( max_elem_size )
NETGEN_3D_Parameters_1.SetMinSize( min_elem_size )
NETGEN_3D_Parameters_1.SetSecondOrder( 0 )
NETGEN_3D_Parameters_1.SetOptimize( 1 )
NETGEN_3D_Parameters_1.SetFineness( 3 )
NETGEN_3D_Parameters_1.SetChordalError( -1 )
NETGEN_3D_Parameters_1.SetChordalErrorEnabled( 0 )
NETGEN_3D_Parameters_1.SetUseSurfaceCurvature( 1 )
NETGEN_3D_Parameters_1.SetFuseEdges( 1 )
NETGEN_3D_Parameters_1.SetQuadAllowed( 0 )
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(em_system, em_elem_size)
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(layer0, layer_elem_size)
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(air0, air0_elem_size)
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(of1, of1_elem_size)
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(ch1, far_elem_size)
NETGEN_3D_Parameters_1.SetLocalSizeOnShape(belt0, belt0_elem_size)
NETGEN_3D_Parameters_1.SetCheckChartBoundary( 0 )
openfoam_zone_1 = mesh.GroupOnGeom(openfoam_zone,'openfoam_zone',SMESH.VOLUME)
em_system_1 = mesh.GroupOnGeom(em_system,'em_system',SMESH.VOLUME)
air_1 = mesh.GroupOnGeom(air,'air',SMESH.VOLUME)
ext_1 = mesh.GroupOnGeom(ext,'ext',SMESH.FACE)
isDone = mesh.Compute()
[ openfoam_zone_1, em_system_1, air_1, ext_1 ] = mesh.GetGroups()

#export the mesh
try:
  mesh.ExportUNV( cdir+'/mesh.unv' )
  pass
except:
  print('ExportUNV() failed. Invalid file name?')


## Set names of Mesh objects
smesh.SetName(NETGEN_1D_2D_3D.GetAlgorithm(), 'NETGEN 1D-2D-3D')
smesh.SetName(NETGEN_3D_Parameters_1, 'NETGEN 3D Parameters_1')
smesh.SetName(ext_1, 'ext')
smesh.SetName(mesh.GetMesh(), 'mesh')
smesh.SetName(air_1, 'air')
smesh.SetName(em_system_1, 'em_system')
smesh.SetName(openfoam_zone_1, 'openfoam_zone')

import killSalome
killSalome.killAllPorts()
