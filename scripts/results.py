import user_data
from shutil import copyfile
import os
cdir = os.getcwd()
if not os.path.exists(cdir+"/images"):
	os.mkdir(cdir+"/images")

vtklist = []

#load the list of time values
_, timename, _ = next(os.walk(cdir+'/postProcessing/metal_surface'))

latest_file = int(len(timename))
timename = sorted(timename, key=float)
latest_time = timename[int(len(timename))-1]

#copy and rename the vtk files from the time folders
for i in range(latest_file):
	vtk0 = "postProcessing/metal_surface/"+timename[i]+"/beta_freesurface.vtk"
	vtk1 = "postProcessing/metal_surface/beta_freesurface"+str(i)+".vtk"
	vtklist.append(vtk1)
	if os.path.exists(vtk0):
		copyfile(vtk0,vtk1)

from paraview.simple import *
paraview.simple._DisableFirstRenderCameraReset()

renderView1 = GetActiveViewOrCreate('RenderView')
layout1 = GetLayout()
fact = 2
image_res = [fact*1000, fact*350]
layout1.SetSize(image_res)

# camera placement
xcam = 0.5*user_data.belt_length
ycam = 150*user_data.inlet_flowrate/(user_data.density*user_data.belt_speed)
zcam = 0.5*user_data.belt_length+user_data.width
renderView1.CameraPosition = [xcam,ycam,zcam]
renderView1.CameraFocalPoint = [0.5*user_data.belt_length, 0, 0]
renderView1.CameraViewUp = [0, 1, 0]
renderView1.CameraParallelScale = 1

# show em box
em = Box(registrationName='Box1')
em.XLength = user_data.em_length
em.YLength = user_data.em_height
em.ZLength = user_data.width
em.Center = [user_data.inlet_gap+0.5*em.XLength,-user_data.em_y0-0.5*em.YLength, 0.0]
emDisplay = Show(em, renderView1, 'GeometryRepresentation')
emDisplay.DiffuseColor = [0.3, 0.0, 0.5]

# load the belt boundary
belt0 = OpenFOAMReader(registrationName='casting.foam', FileName=cdir+'/casting.foam')
belt0.MeshRegions = ['belt']
belt0Display = Show(belt0, renderView1, 'GeometryRepresentation')
belt0Display.Representation = 'Surface'

#create belt color
Hide(belt0, renderView1)
belt_div = 10
freq = belt_div*user_data.belt_speed/user_data.belt_length
wav = user_data.belt_length/belt_div
belt1 = Calculator(registrationName='belt1', Input=belt0)
belt1.ResultArrayName = 'belt_mov'
belt1.Function = 'cos(2*acos(-1)*coordsX/%f)'%wav
belt1Display = Show(belt1, renderView1, 'GeometryRepresentation')
belt_movLUT = GetColorTransferFunction('belt_mov')
belt1Display.Representation = 'Surface'
belt1Display.ColorArrayName = ['POINTS', 'belt_mov']
belt1Display.SetScalarBarVisibility(renderView1, True)
belt_movPWF = GetOpacityTransferFunction('belt_mov')
belt_movLUT.RescaleTransferFunction(-1,1)
belt_movPWF.RescaleTransferFunction(-1,1)
belt_movLUT.RGBPoints = [-1,0.2,0.2,0.2, 0,0.6,0.6,0.6, 1,1,1,1]
#belt_movLUT.ApplyPreset('X Ray', True)
belt1Display.Opacity = 1
belt1Display.SetScalarBarVisibility(renderView1, False)

# load the vtk files
beta_freesurface = LegacyVTKReader(registrationName='beta_freesurface', FileNames=vtklist)
animationScene1 = GetAnimationScene()
animationScene1.UpdateAnimationUsingDataTimeSteps()
beta_freesurfaceDisplay = Show(beta_freesurface, renderView1, 'GeometryRepresentation')
beta_freesurfaceDisplay.Representation = 'Surface'
ColorBy(beta_freesurfaceDisplay, ('POINTS', 'beta'))
beta_freesurfaceDisplay.RescaleTransferFunctionToDataRange(True, False)
beta_freesurfaceDisplay.SetScalarBarVisibility(renderView1, True)
betaLUT = GetColorTransferFunction('beta')
betaLUT.RescaleTransferFunction(0.5, 1.0)
beta_freesurfaceDisplay.SetScalarBarVisibility(renderView1, False)

# create time text
text1 = Text(registrationName='Text1')
text1.Text = 'Time = s'
text1Display = Show(text1, renderView1, 'TextSourceRepresentation')
text1Display.WindowLocation = 'UpperCenter'
text1Display.FontSize = 40
text1Display.FontFamily = 'Times'
SetActiveSource(beta_freesurface)

# save liquid fraction images
for i in range(latest_file):
	belt1.Function = 'cos(-2*acos(-1)*%f+2*acos(-1)*coordsX/%f)'%(freq*float(timename[i]),wav)
	text1.Text = 'Time = '+timename[i]+' s'
	animationScene1.AnimationTime = i
	SaveScreenshot(cdir+"/images/"+"liquid_fraction_"+str(i)+
	'.png', layout1, ImageResolution=image_res)

Hide(beta_freesurface, renderView1)

# get y range of the metal strip
clip1 = Clip(registrationName='Clip1', Input=beta_freesurface)
clip1.ClipType.Origin = [2*user_data.inlet_gap, 0.0, 0.0]
clip1.ClipType.Normal = [-1.0, 0.0, 0.0]
calculator1 = Calculator(registrationName='Calculator1', Input=clip1)
calculator1.ResultArrayName = 'y_mm'
calculator1.Function = 'coordsY*1000'
h_range = calculator1.PointData.GetArray("y_mm").GetRange()

# display the metal surface colored by height
calculator2 = Calculator(registrationName='Calculator2', Input=beta_freesurface)
calculator2.ResultArrayName = 'thickness'
calculator2.Function = 'coordsY*1000'
calculator2Display = Show(calculator2, renderView1, 'UnstructuredGridRepresentation')
calculator2Display.Representation = 'Surface'
calculator2Display.SetScalarBarVisibility(renderView1, True)
thicknessLUT = GetColorTransferFunction('thickness')
thicknessLUT.RescaleTransferFunction(0, h_range[1])

# create a color bar
thicknessLUTColorBar = GetScalarBar(thicknessLUT, renderView1)
thicknessLUTColorBar.TitleFontSize = 40
thicknessLUTColorBar.LabelFontSize = 40
thicknessLUTColorBar.AutomaticLabelFormat = 0
thicknessLUTColorBar.LabelFormat = '%.1f'
thicknessLUTColorBar.RangeLabelFormat = '%.1f'
thicknessLUTColorBar.TitleFontFamily = 'Times'
thicknessLUTColorBar.WindowLocation = 'AnyLocation'
thicknessLUTColorBar.Position = [0.85, 0.60]
thicknessLUTColorBar.ScalarBarLength = 0.35

# save layer thickness images
for i in range(latest_file):
	belt1.Function = 'cos(-2*acos(-1)*%f+2*acos(-1)*coordsX/%f)'%(freq*float(timename[i]),wav)
	text1.Text = 'Time = '+timename[i]+' s'
	animationScene1.AnimationTime = i
	SaveScreenshot(cdir+"/images/"+"layer_thickness_"+str(i)+
	'.png', layout1, ImageResolution=image_res)
