import imageio

from shutil import copyfile
import os
cdir = os.getcwd()

lf_images = []
lt_images = []

_, timename, _ = next(os.walk(cdir+'/postProcessing/metal_surface'))
latest_file = int(len(timename))

for i in range(latest_file):
	lt_images.append(imageio.imread(cdir+"/images/"+"layer_thickness_"+str(i)+'.png'))
	lf_images.append(imageio.imread(cdir+"/images/"+"liquid_fraction_"+str(i)+'.png'))

imageio.mimsave(cdir+"/layer_thickness.gif", lt_images)
imageio.mimsave(cdir+"/liquid_fraction.gif", lf_images)
