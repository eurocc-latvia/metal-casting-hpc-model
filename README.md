# Summary

This demo case represents a novel continuous casting method where liquid metal is poured directly onto an intensively-cooled moving belt. The liquid metal solidifies forming a thin solid strip that is continuously pulled away by the belt. Traveling magnetic field is used at the beginning of the belt to control the speed of the liquid metal and synchronise it with the belt speed. More information about the casting process can be found [here](https://doi.org/10.1002/srin.200300256), [here](https://doi.org/10.1002/srin.201300105) and [here](https://op.europa.eu/en/publication-detail/-/publication/f737caf3-4071-49fc-b616-2d45e6f46df8).

The demo model includes electromagnetic field solved by [*Elmer FEM*](https://www.elmerfem.org) and fluid dynamics, heat transfer and solidification solved by [*OpenFOAM*](https://openfoam.org). Coupling (data exchange and interpolation) between *Elmer* and *OpenFOAM* is achieved using the *EOF-Library* (code [here](https://github.com/jvencels/EOF-Library) and technical paper [here](https://doi.org/10.1016/j.softx.2019.01.007)).

The main goal of this demo model is to demonstrate efficient numerical solution of a complex multiphysical process using high-performance computing. Based on the physical processes considered in the model, the main target audience are people, companies and institutions working in the field of metallurgy, especially employing or studying the use of magnetic field for solidification, melting and casting process control.

**This HPC demonstration was prepared as a part of the training and dissemination activities organized by the [EuroCC National HPC Competence Centre of Latvia](https://eurocc-latvia.lv/?lang=en).**

# Table of contents

[[_TOC_]]


# Problem description

Liquid metal at constant temperature is pouring down from the inlet onto the belt. The belt is moving at constant speed from left to right in the scheme. The belt is assigned a constant cooling rate in Watts per square meter. The liquid metal gradually solidifies on the belt and is pulled away from the domain at the belt speed.

*OpenFOAM* part of the model contains only the region above the belt with liquid and solid metal and some air. *OpenFOAM* solves time-dependent fluid flow, heat transfer and solidification. The `JxB` field is the electromagnetic force received from *Elmer* acting on the liquid metal.

*Elmer* part of the model contains the whole *OpenFOAM* region, as well as the traveling magnetic field system and some surrounding air. *Elmer* solves steady electromagnetics in frequency domain, which means that force acting on the metal can be viewed as time-average.

During the simulation, *OpenFOAM* transfers electrical conductivity distribution (which corresponds to the metal) to *Elmer*, which then returns to *OpenFOAM* updated electromagnetic force distribution.

Scheme of the model is shown below, with the parameters as defined in `user_data.py`.

<img src="scheme.png" width="1100">

System width in Z direction is the `width` parameter in `user_data.py`. Inlet and channel height are determined by the average cast strip thickness `h = inlet_flowrate/(density*belt_speed*width)`. Inlet height is `4h` and channel height is `3h`.

Ensure that all geometry parameters are positive and greater than zero and that `em_x0 > inlet_gap` and `belt_length > (inlet_gap+2*em_length)`, otherwise the meshing fails.


# Case structure

The demo case contains files and folders for *Elmer* and *OpenFOAM*, as well as multiple scripts for simulation automation. The following folders contain files for *OpenFOAM*:
* `0` - Initial and boundary conditions for all fields.
* `constant` - Material properties, physical constants and some numerical model parameters.
* `system` - Definition of simulation time data, linear system solvers, discretisation schemes.

*Elmer* case file is the following:
* `case.sif` - Contains all case parameters

The file `user_data.py` contains the main parameters (geometry data, material properties etc) for easy user access and editing. Other parameters can be accessed by editing other files (e.g. EM system parameters can be changed in the *Elmer* case file `case.sif`).

The `scripts` folder contains the following files and folders:
* `mhdSolidificationInterFoam` - Folder containing custom *OpenFOAM* solver with coupling to *Elmer*.
* `animation.py` - Script that generates GIF animations from the images generated in ParaView.
* `prepare_data.py` - Script that prepares case data using the variables in `user_data.py`.
* `results.py` - Postprocessing script used by *ParaView* to generate images after the simulation.
* `salome_input_template.py` - Template for *Elmer* mesh generation in *Salome*.

The file `solve` contains all commands for case preparation, running simulations and postprocessing results. Contents of it are described below.


# Software requirements

The demo case requires Linux environment and the following software:
* ***Git*** - To obtain this case directly from *GitLab* via the `clone` command. Alternatively, code can be downloaded as `.zip` or `.tar` archive and copied to the HPC cluster by other means.
* ***Python*** - Used for case data preparation (`scripts/prepare_data.py` script), as well as for mesh generation in *Salome* and postprocessing in *ParaView*. Optionally, the `imageio` library is required for generating animations (`scripts/animation.py` script).
* ***OpenMPI*** - Used for parallel processing. Versions 2.1.1 and 4.0.0 were tested.
* ***OpenFOAM v6*** - For the simulation of fluid dynamics, heat transfer and solidification.
* ***Elmer FEM*** - For the electromagnetics simulation.
* ***EOF-Library*** - To couple *Elmer* and *OpenFOAM*.
* ***Salome v9.0.0+*** - For *Elmer* mesh generation. Version 9.6.0 was tested.
* ***ParaView*** - For postprocessing and image generation. Version 5.6.1 was tested.




# Running the demo

To obtain the case, execute in the terminal:
```
git clone https://gitlab.com/eurocc-latvia/metal-casting-hpc-model.git
```
The case files are then located in `metal-casting-hpc-model` folder. Go to that folder by running:
```
cd metal-casting-hpc-model
```

If all the required software is installed/loaded and no workload or job management system is used, run the simulation by executing:
```
./solve
```
This executes the commands within the `solve` script as explained below.

If the computer or cluster uses the [*Slurm Workload Manager*](https://slurm.schedmd.com/documentation.html), to submit the simulation job, run
```
sbatch [-options] solve
```
where `[-options]` are Slurm parameters such as `--ntasks=48` etc. The Slurm options can be included in the `solve` script to avoid writing them in the command terminal. In that case, the command to submit the Slurm job would be simply
```
sbatch solve
```
An example of Slurm parameters included in the `solve` script to run the demo case on the University of Latvia HPC cluster is given below.

## Slurm example

The demo case was mostly tested on the University of Latvia HPC cluster which uses the Slurm system and software is loaded as modules before simulations. To run the case on this cluster, the following lines are added at the beginning of the `solve` script:

```
#!/bin/bash
#SBATCH --job-name=eucc         # job name
#SBATCH --partition=regular     # partition name
#SBATCH --nodes=1-1             # number of nodes to use (min-max)
#SBATCH --ntasks=48             # number of cores
#SBATCH --time=10-0             # time limit d-h
#SBATCH --mem=150G              # required memory

module purge
module load gnu9/9.4.0
module load openmpi4/4.1.1
module load eof/eof-6
source /software/openfoam/OpenFOAM-6/etc/bashrc
source /software/eof/EOF-Library-6/etc/bashrc
module load salome/salome-9.6.0
module load paraview/paraview-5.9.1
```

The `#SBATCH` lines define Slurm parameters. In this example, 48 cores are requested on any single `regular` node. Without hyperthreading, the current cluster configuration allows running only one process on each core, which means that in this example we must set
```
nProc=24
```
to utilize the requested 48 cores (*Elmer* and *OpenFOAM* each will use 24 cores).

After the `#SBATCH` lines the necessary programs are loaded. `eof` module contains *Elmer*, *OpenFOAM* and *EOF-Library*. *OpenFOAM* and *EOF-Library* environment variables are loaded by the two `source` commands. And then *Salome* and *ParaView* are loaded. The rest of the `solve` script commands are explained below.


## The `solve` script contents

The first two commands create a variable `nProc` that is the number of processors to use for parallel processing and assigns the value in the *OpenFOAM* file `system/decomposeParDict`. The same number of processors will be used for both *Elmer* and *OpenFOAM* simulation.
```
nProc=24
sed -i "s/numberOfSubdomains [^;]*/numberOfSubdomains $nProc/" system/decomposeParDict
```

Then, a custom *OpenFOAM* solver `mhdSolidificationInterFoam` is compiled by running
```
wclean scripts/mhdSolidificationInterFoam
wmake scripts/mhdSolidificationInterFoam
```

Case data is prepared by running
```
python3 scripts/prepare_data.py
```
The command above loads `user_data.py` and creates new files: `openfoam_data` - data for *OpenFOAM* mesh, material properties, boundary conditions etc., `elmer_data` - data for *Elmer* case and `salome_input.py` - script for *Elmer* mesh generation in *Salome*.

Next, *Elmer* mesh is generated in *Salome*, converted from UNV to *Elmer* format and decomposed in the number of processors `nProc` defined above:
```
salome -t salome_input.py &> salome.log
ElmerGrid 8 2 mesh.unv
ElmerGrid 2 2 mesh -metis $nProc
```
The `salome` command calls the *Salome* program. If for some reason the Salome executable is called differently, it must be changed in the command above accordingly.

Then the *OpenFOAM* mesh is generated and decomposed for parallel processing by running:
```
blockMesh
setFields
decomposePar -force
```
The `blockMesh` utility generates the mesh, `setFields` initializes the fields defined in `system/setFieldsDict` (in this case it is the `em_zone` field which determines the zone relevant for *Elmer*-*OpenFOAM* coupling) and `decomposePar` divides the mesh in `nProc` parts creating a folder for each processor core (`processor0`, `processor1` etc).

Finally, the simulation is started by executing
```
mpirun -np $nProc mhdSolidificationInterFoam -parallel : -np $nProc ElmerSolver_mpi case.sif
```
The command above means that `mhdSolidificationInterFoam` and `ElmerSolver_mpi` executables are run together using `mpirun`. In this case, using *EOF-Library*, the simulation proceeds by alternately running *OpenFOAM* and *Elmer* and exchanging data between them. The frequency of the switching between *Elmer* and *OpenFOAM* is determined by the `maxRelDiffAlpha` and `maxRelDiffU` parameters (maximum relative difference of the volume fraction of metal and liquid metal velocity in any mesh cell between the current time-step and the previous coupling iteration). If `maxRelDiffAlpha>1`, electromagnetic field will be calculated only at the start and end of the simulation.

# User modifications

To make adjusting the most relevant parameters easier, they are collected in the file `user_data.py`. That file contains multiple categories of parameters with comments above each category. For example, geometry size section in `user_data.py` is
```
#geometry data
inlet_gap = 0.01
belt_length = 0.5
em_x0 = 0.02
em_y0 = 0.005
em_height = 0.02
em_length = 0.15
width = 0.2
```
where the line starting with \# symbol is a comment. These parameters are shown in the model scheme above. To change the sizes, simply change the numbers assigned to each parameter before starting the simulation. Most parameters in the file are self-explanatory due to their name. The characteristic mesh element sizes at the end of `user_data.py` are approximate values. See `scripts/prepare_data.py`, `scripts/salome_input_template.py` and `system/blockMeshDict` to find out how exactly these mesh sizes are used.

## Modifications outside of `user_data.py`

Parameters and settings not available in `user_data.py` can be modified by editing other files. For example, to change discretisation schemes and solution convergence criteria, see `system/fvSchemes` and `system/fvSolution`, respectively. To change boundary condition types in *OpenFOAM*, edit the fields in the `0` folder.

Everything can be edited and changed, but there is no guarantee that the simulation would run correctly with all changes. Be especially careful with geometry sizes (see comments below the model scheme).


# Results

Images and animations of the liquid fraction and layer thickness will be generated after the simulation. An example of the images using the default parameters is shown below. Liquid fraction is the top image with blue being solid and red being liquid metal.

<img src="example_results.png" width="600">
